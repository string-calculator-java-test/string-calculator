package com.example.test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class StringCalculator {

	 public static int add(String nums) {
	        if (nums == null || nums.isEmpty()) {
	            return 0;
	        }
	        
	        String str = ",";
	        String[] numStrings = nums.split("\n");
	        
	        if (numStrings[0].startsWith("//")) {
	        	str = numStrings[0].substring(2);
	            numStrings = Arrays.copyOfRange(numStrings, 1, numStrings.length);
	        }
	        
	        List<Integer> negativeNumbers = new ArrayList<>();
	        int sum = 0;
	        
	        for (String numString : numStrings) {
	            String[] numArray = numString.split(Pattern.quote(str));
	            
	            for (String num : numArray) {
	                int parsedNumber = Integer.parseInt(num);
	                
	                if (parsedNumber < 0) {
	                    negativeNumbers.add(parsedNumber);
	                }
	                
	                if (parsedNumber <= 1000) {
	                    sum += parsedNumber;
	                }
	            }
	        }
	        
	        if (!negativeNumbers.isEmpty()) {
	            throw new IllegalArgumentException("negatives not allowed: " + negativeNumbers.toString());
	        }
	        
	        return sum;
	    }
	    
	    public static void main(String[] args) {
	        System.out.println(add("")); 
	        System.out.println(add("1,2,3")); 
	        System.out.println(add("1\n2,3")); 
	        System.out.println(add("//;\n1;2")); 
	        System.out.println(add("//-\n1-2-3")); 
	        
	        try {
	            System.out.println(add("1,\n")); 
	        } catch (IllegalArgumentException e) {
	            System.out.println(e.getMessage()); 
	        }
	        
	        try {
	            System.out.println(add("-1,2,-3")); 
	        } catch (IllegalArgumentException e) {
	            System.out.println(e.getMessage()); 
	        }
	    }
}
